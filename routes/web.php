<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/','PaymentController@index');

Route::post('payment/pay','PaymentController@pay');

Route::get('payment/confirmation', 'PaymentController@confirmation');

Route::get('payment/failed','PaymentController@paymentFail');

Route::get('payment/success', 'PaymentController@paymentSuccess');
