<html>
<head>
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
</head>
<body>
    
        @if ($message = Session::get('success'))
  
            <p>{!! $message !!}</p>
       
        <?php Session::forget('success');?>
        @endif

        @if ($message = Session::get('error'))
       
            <p>{!! $message !!}</p>
        
        <?php Session::forget('error');?>
        @endif

<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="post" id="payment-form" action="{{url('payment/pay')}}" onsubmit="payment(this,event)">
    	  <div class="w3-container w3-teal w3-padding-16">Paywith Paypal</div>
    	  {{ csrf_field() }}
    	  <h2 class="w3-text-blue">Payment Form</h2>
    	  <p>Demo PayPal form - Integrating paypal in laravel</p>
    	  <label class="w3-text-blue"><b>Enter Amount</b></label>
    	  <input class="w3-input w3-border" id="amount" type="text" name="amount"></p>
    	  <button class="w3-btn w3-blue">Pay with PayPal</button>
</form>

<script>
var payment=function(el,event){
	// data: {
	// 				_token: $('meta[name="csrf-token"]').attr('content'),
	// 				data: JSON.stringify(user)
	// 			},
	event.preventDefault();
	$.ajax({
				url: $(el).attr('action'),
				type: $(el).attr('method'),
				data: $(el).serialize(),
				
				beforeSend: function () {
					$('.ui-loading').show();
				},
				complete: function () {
					$('.ui-loading').hide();
				},
				success: function (resp) {
					alert(resp);
				},
				error: function (error) {
						alert("Erreur lors d'une opération (error ajax)");
				}
			}); // ./ajax
}

</script>
    
</body>
</html>